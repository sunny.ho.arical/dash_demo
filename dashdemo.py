from flask import Flask,request, render_template ,jsonify,redirect,url_for,send_from_directory
import dash
import dash_core_components as dcc                  
import dash_html_components as html
from dash.dependencies import Input, Output,State
import pandas as pd
from statistics import mean 
#import js
external_scripts = ['https://d3js.org/d3.v5.min.js','https://cdnjs.cloudflare.com/ajax/libs/d3-cloud/1.2.5/d3.layout.cloud.min.js','https://cdnjs.cloudflare.com/ajax/libs/d3-tip/0.9.1/d3-tip.js']
#create flask server
server = Flask(__name__)
#create a dash with flask
app = dash.Dash(
    __name__,
    server=server,
    routes_pathname_prefix='/',
    external_scripts=external_scripts
)

df = pd.DataFrame([{'StudentName':"Sunny","ITP4864":100,"ITP4865":89},{'StudentName':"Tom","ITP4864":65,"ITP4865":70}])
#plotly graph


#body content
app.layout = html.Div([
    dcc.Graph(
            id='g1',
            figure = dict(
                data = [{'x': df['StudentName'], 'y':[mean(i[1:]) for i in df.values] , 'type': 'bar'}],
                layout = dict(title = 'Dash demo',clickmode = 'event+select'))
            ),dcc.Graph(id = 'g2')
],id = 'page')
#event setting
@app.callback(
    Output("g2", 'figure'),
    [Input('g1','selectedData')]
)
def myfun(x):
    return {'data' : [{'x': list(df[df['StudentName']== "Sunny"].iloc[:,1:].columns), 'y':list(df[df['StudentName']== j['x']].iloc[:,1:].values[0]) , 'type': 'bar'} for j in x['points']]}
    
   
            
            

if __name__ == '__main__':
    server.run(host="127.0.0.1", port=80,debug=True)

